var cancion=new Audio("multimedia/audio/diferencias.mp3");
var enMarcha=false;
var silenciado=false;

function play(){

    if(!enMarcha){

        cancion.play();
        document.getElementById("play").src="multimedia/img/pause.png";
        document.getElementById("play").title="Pause";
        enMarcha=true;
    }else{

        cancion.pause();
        document.getElementById("play").src="multimedia/img/play.png";
        document.getElementById("play").title="Play";
        enMarcha=false;
    }


}

function volumen(aumento){

    if(cancion.volume+aumento>1 || cancion.volume+aumento<0 )return;
    cancion.volume+=aumento;

}

function silenciar(){

    if(silenciado){
        cancion.muted=false;
        document.getElementById("mute").src="multimedia/img/silenciar.png";
        document.getElementById("mute").title="Silenciar";
        silenciado=false;
    }else{
        cancion.muted=true;
        document.getElementById("mute").src="multimedia/img/desilenciar.png";
        document.getElementById("mute").title="Activar altaveu";
        silenciado=true;
    }

}

function stop(){

    cancion.pause();
    document.getElementById("play").src="multimedia/img/play.png";
    enMarcha=false;
    cancion.currentTime=0;
}
