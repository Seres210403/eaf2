var canvas, context;
canvas = document.getElementById('imageView');
context = canvas.getContext('2d');

context.lineWidth = 3;

context.strokeStyle = '#000000';
context.save();
context.translate(306, 252);
context.scale(1, 0.9875);
context.beginPath();
context.arc(0, 0, 225, 0, 6.283185307179586, false);
context.stroke();
context.closePath();
context.restore();

context.strokeStyle = '#000000';
context.save();
context.translate(231.5, 161.5);
context.scale(1, 0.5238095238095238);
context.beginPath();
context.arc(0, 0, 24, 0, 6.283185307179586, false);
context.stroke();
context.closePath();
context.restore();

context.strokeStyle = '#000000';
context.save();
context.translate(368, 154.5);
context.scale(1, 0.4090909090909091);
context.beginPath();
context.arc(0, 0, 24, 0, 6.283185307179586, false);
context.stroke();
context.closePath();
context.restore();

context.strokeStyle = '#000000';
context.save();
context.translate(301.5, 223.5);
context.scale(0.09090909090909091, 1);
context.beginPath();
context.arc(0, 0, 55, 0, 6.283185307179586, false);
context.stroke();
context.closePath();
context.restore();

context.strokeStyle = '#000000';
context.save();
context.translate(302.5, 363);
context.scale(1, 0.12612612612612611);
context.beginPath();
context.arc(0, 0, 112, 0, 6.283185307179586, false);
context.stroke();
context.closePath();
context.restore();
