// Llista de reproducció

var videoList = [{
  sources: [{
    src: 'multimedia/video/bisbal.mp4',
    type: 'video/mp4'
  }],
  poster: 'multimedia/img/bisbal.jpg'
}, {
  sources: [{
    src: 'multimedia/video/jovi.mp4',
    type: 'video/mp4'
  }],
  poster: 'multimedia/img/american.jpg'
}, {
  sources: [{
    src: 'multimedia/video/palo.mp4',
    type: 'video/mp4'
  }],
  poster: 'multimedia/img/Pau.jpg'
}, {
  sources: [{
    src: 'multimedia/video/queen.mp4',
    type: 'video/mp4'
  }],
  poster: 'multimedia/img/lambert.jpg'
}];

// Creo el reproductor
var player = videojs(document.querySelector('video'), {
  inactivityTimeout: 0
});

// Ajusto el volum inicial del reproductor
  player.volume(5);

// Afegeixo dades perque per consola es pugui veure les accions que està realitzant el reproductor

player.on([
  'duringplaylistchange', // Canvi en la duració de la play list, apareix quan es crea la nova llista
  'playlistchange', // Canvi en la playlist. No apareix mai perquè un cop creada no fem canvis
  'beforeplaylistitem', // Canvi de l'item de la llista sel.leccionat, apareix quan fem click a anterior o següent.
  'playlistitem', // Apareix quan sel.leccionem un item de la llista.
  'playlistsorted' // Apareix quan ordenem la llista, en aquesta pàgina no apareixerà mai perquè no l'ordenem mai.
], function(e) {
  videojs.log('player saw "' + e.type + '"'); // Envia a consola la informació.
});

// Afegim la llista al reproductor
player.playlist(videoList);

// Cridem al mètode previous quan pulsem el botó anterior
document.querySelector('.previous').addEventListener('click', function() {
  player.playlist.previous();
});
// Cridem al mètode next quan pulsem el botó següent
document.querySelector('.next').addEventListener('click', function() {
  player.playlist.next();
});

// Posem les propietats de autoadvance als elements de la llista afegint-hi un event
Array.prototype.forEach.call(document.querySelectorAll('[name=autoadvance]'), function(el) {
  el.addEventListener('click', function() {
    var value = document.querySelector('[name=autoadvance]:checked').value;
    player.playlist.autoadvance(Number(value));
  });
});

//El posem inicialment en no autoAdvance
document.querySelector('[name="autoadvance"][value="null"]').click();

// Afegim un event al checkbox repetir.
var repeatCheckbox = document.querySelector('.repeat');
repeatCheckbox.addEventListener('click', function() {
  player.playlist.repeat(this.checked);
});
// Posem inicialment el checkbox repetir com desmarcat
repeatCheckbox.checked = false;
