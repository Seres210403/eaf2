var tiempos=[5000, 2000, 5000];
var colorActual=0;
var tiempo=tiempos[colorActual];
colorRojo();
setInterval(cambiarColeres, 1000);

function cambiarColeres(){
  // Control de tiempos
  tiempo=tiempo-1000;
  if(tiempo==0){
    colorActual++;
    if(colorActual==3)colorActual=0
    tiempo=tiempos[colorActual];
  }

  // pintar circulos
  switch (colorActual) {
    case 0:
      colorRojo();
      break;
    case 1:
      colorNaranja();
      break;
    case 2:
      colorVerde();
      break;
  }
}

function colorRojo(){
  $("#naranja").css("background","grey");
  $("#rojo").css("background","red");
  $("#verde").css("background","grey");

}

function colorNaranja(){

  $("#verde").css("background","grey");
  $("#naranja").css("background","orange");
  $("#rojo").css("background","grey");
}

function colorVerde(){

  $("#rojo").css("background","grey");
  $("#naranja").css("background","grey");
  $("#verde").css("background","green");
}
